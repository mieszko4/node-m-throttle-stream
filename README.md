# m-throttle-stream
> Transformation stream that throttles output stream.

## Requirements

* ```git```
* ```npm``` and ```node.js```

## Install

```sh
$ npm install bitbucket:mieszko4/node-m-throttle-stream
```


## Usage

```js
var ThrottleStream = require('m-throttle-stream');
var Readable = require('stream').Readable;

var source = new Readable({encoding: 'utf8'});
var destination = new ThrottleStream({
  bufferSize: 10,
  maxTimeout: 1000,
  encoding: 'utf8'
});

destination.on('data', function (chunk) {
  console.log(chunk);
});

source.pipe(destination);
source.push('abcdefghij');
source.push('ABCDEFGHIJ');
source.push(null);
```

## Development


```sh
$ git clone https://mieszko4@bitbucket.org/mieszko4/node-m-throttle-stream.git
$ cd node-m-throttle-stream
$ npm install
$ npm test
```

## TODO

* Figure out how to close Transport stream explicitly in order to have greater control over pushing data

## License

Apache-2.0 © [Mieszko4]()