'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _stream = require('stream');

var _mCircularBuffer = require('m-circular-buffer');

var _mCircularBuffer2 = _interopRequireDefault(_mCircularBuffer);

function ThrottleStream() {
  var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

  var _ref$maxTimeout = _ref.maxTimeout;
  var maxTimeout = _ref$maxTimeout === undefined ? 1000 : _ref$maxTimeout;
  var _ref$maxRate = _ref.maxRate;
  var maxRate = _ref$maxRate === undefined ? 32 : _ref$maxRate;
  var _ref$bufferSize = _ref.bufferSize;
  var bufferSize = _ref$bufferSize === undefined ? 64 : _ref$bufferSize;
  var _ref$encoding = _ref.encoding;
  var encoding = _ref$encoding === undefined ? false : _ref$encoding;
  var _ref$objectMode = _ref.objectMode;
  var objectMode = _ref$objectMode === undefined ? false : _ref$objectMode;

  if (!(this instanceof ThrottleStream)) {
    return new ThrottleStream();
  }

  var options = {};
  if (objectMode) {
    options.readableObjectMode = true;
    options.writableObjectMode = true;
  }
  if (encoding) {
    options.encoding = encoding;
  }
  _stream.Transform.call(this, options);

  this._maxTimeout = maxTimeout;
  this._maxRate = maxRate;
  this._objectMode = objectMode;
  if (objectMode) {
    this._bufferSize = bufferSize;
  } else {
    this._buffer = new _mCircularBuffer2['default'](bufferSize);
  }
}

_util2['default'].inherits(ThrottleStream, _stream.Transform);

var getTimeout = function getTimeout() {
  var timeout = Math.floor(Math.random() * this._maxTimeout);

  return timeout;
};

var getRate = function getRate(dataLength) {
  var rate = Math.round(Math.random() * (dataLength - 1) + 1);
  rate = Math.min(this._maxRate, rate);

  return rate;
};

var delayBufferPush = function delayBufferPush(done) {
  var _this = this;

  if (this._buffer.length > 0) {
    (function () {
      var rate = getRate.call(_this, _this._buffer.length);
      var timeout = getTimeout.call(_this);

      setTimeout((function () {
        this._buffer.unshift(rate, (function (chunk) {
          this.push(Buffer.concat([chunk]));
        }).bind(this));

        delayBufferPush.call(this, done);
      }).bind(_this), timeout);
    })();
  } else {
    done.call(this);
  }
};

var delayObjectPush = function delayObjectPush(chunk, simulatedChunkSize, done) {
  var timeout = 0;
  var totalRate = 0;

  while (simulatedChunkSize > totalRate) {
    totalRate += getRate.call(this, simulatedChunkSize);
    timeout += getTimeout.call(this);
  }

  setTimeout((function () {
    this.push(chunk);
    done();
  }).bind(this), timeout);
};

//TODO: right now next chunk cannot come until timeout is done. EXPLICITLY end stream!. problem is how to EXPLICITLY end stream!
ThrottleStream.prototype._transform = function (chunk, encoding, done) {
  if (!this._objectMode) {
    if (this._buffer.push(chunk)) {
      //buffer is sufficient
      delayBufferPush.call(this, done); //initialize delay call
    } else {
        //existing buffer is not sufficient, push chunk instantly
        this.push(chunk);
        done();
      }
  } else {
    var simulatedChunkSize = JSON.stringify(chunk).length;
    if (simulatedChunkSize < this._bufferSize) {
      delayObjectPush.call(this, chunk, simulatedChunkSize, done);
    } else {
      //simulated buffer not sufficient, push chunk instantly
      this.push(chunk);
      done();
    }
  }
};

exports['default'] = ThrottleStream;
module.exports = exports['default'];
//ms
//bytes
//bytes