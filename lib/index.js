import util from 'util';
import { Transform } from 'stream';
import CircularBuffer from 'm-circular-buffer';

function ThrottleStream({
  maxTimeout: maxTimeout = 1000, //ms
  maxRate: maxRate = 32, //bytes
  bufferSize: bufferSize = 64, //bytes
  encoding: encoding = false,
  objectMode: objectMode = false
} = {}) {
  if (!(this instanceof ThrottleStream)) {
    return new ThrottleStream();
  }

  let options = {};
  if (objectMode) {
    options.readableObjectMode = true;
    options.writableObjectMode = true;
  }
  if (encoding) {
    options.encoding = encoding;
  }
  Transform.call(this, options);

  this._maxTimeout = maxTimeout;
  this._maxRate = maxRate;
  this._objectMode = objectMode;
  if (objectMode) {
    this._bufferSize = bufferSize;
  } else {
    this._buffer = new CircularBuffer(bufferSize);
  }
}

util.inherits(ThrottleStream, Transform);

let getTimeout = function () {
  let timeout = Math.floor(Math.random() * this._maxTimeout);

  return timeout;
};

let getRate = function (dataLength) {
  let rate = Math.round(Math.random() * (dataLength - 1) + 1);
  rate = Math.min(this._maxRate, rate);

  return rate;
};

let delayBufferPush = function (done) {
  if (this._buffer.length > 0) {
    let rate = getRate.call(this, this._buffer.length);
    let timeout = getTimeout.call(this);

    setTimeout(function () {
      this._buffer.unshift(rate, function (chunk) {
        this.push(Buffer.concat([chunk]));
      }.bind(this));

      delayBufferPush.call(this, done);
    }.bind(this), timeout);
  } else {
    done.call(this);
  }
};

let delayObjectPush = function (chunk, simulatedChunkSize, done) {
  let timeout = 0;
  let totalRate = 0;

  while (simulatedChunkSize > totalRate) {
    totalRate += getRate.call(this, simulatedChunkSize);
    timeout += getTimeout.call(this);
  }

  setTimeout(function () {
    this.push(chunk);
    done();
  }.bind(this), timeout);
};

//TODO: right now next chunk cannot come until timeout is done. EXPLICITLY end stream!. problem is how to EXPLICITLY end stream!
ThrottleStream.prototype._transform = function (chunk, encoding, done) {
  if (!this._objectMode) {
    if (this._buffer.push(chunk)) { //buffer is sufficient
      delayBufferPush.call(this, done); //initialize delay call
    } else { //existing buffer is not sufficient, push chunk instantly
      this.push(chunk);
      done();
    }
  } else {
    let simulatedChunkSize = JSON.stringify(chunk).length;
    if (simulatedChunkSize < this._bufferSize) {
      delayObjectPush.call(this, chunk, simulatedChunkSize, done);
    } else { //simulated buffer not sufficient, push chunk instantly
      this.push(chunk);
      done();
    }
  }
};

export default ThrottleStream;
