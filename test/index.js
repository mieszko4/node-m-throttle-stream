import assert from 'assert';
import ThrottleStream from '../lib';
import { Readable } from 'stream';

let getNext = function (array, indices) {
  let forever = true;
  while (forever) {
    if (indices[0] < array.length) {
      let e = array;
      let reset = false;
      for (let i = 0; i < indices.length - 1; i++) {
        e = e[indices[i]];

        if (!Array.isArray(e) && !(e instanceof Buffer) && !(typeof e === 'string')) {
          indices[i]++;
          return e;
        }

        if (indices[i + 1] === e.length) {
          indices[i]++;
          indices[i + 1] = 0;
          reset = true;
          break;
        }
      }

      if (!reset) { //all conditions passed
        return e[indices[indices.length - 1]++];
      }
    } else {
      return undefined;
    }
  }
};

let buffersEqual = function (level, ...buffers) {
  let indicesArray = buffers.map(function () {
    return new Array(level).fill(0);
  });

  let elements;
  let getNextCallback = function (buffer, i) {
    return getNext(buffer, indicesArray[i]);
  };
  let equalConditionCallback = function (element) {
    return elements[0] !== element;
  };
  let endConditionCallback = function (element) {
    return typeof element !== 'undefined';
  };

  do {
    elements = buffers.map(getNextCallback);

    if (elements.some(equalConditionCallback)) {
      return false;
    }
  } while (elements.some(endConditionCallback));

  return true;
};

let assertPipe = function (inputChunks, options, done) {
  let readableOptions = {};
  if (options.objectMode) {
    readableOptions.objectMode = true;
  }

  if (options.encoding) {
    readableOptions.encoding = options.encoding;
  }

  let source = new Readable(readableOptions);
  let actualChunks = [];

  let checkEquality = function () {
    if (options.objectMode) { //expect objects
      assert.deepEqual(inputChunks, actualChunks);
    } else { //expect buffers or strings
      assert(buffersEqual(2, inputChunks, actualChunks));
    }
  };

  let destination = new ThrottleStream(options);
  destination.on('data', function (chunk) {
    actualChunks.push(chunk);
  });
  destination.on('end', function () {
    checkEquality();
    done();
  });
  destination.on('error', function () {
    checkEquality();
    done();
  });

  source.pipe(destination);
  inputChunks.forEach(function (chunk) {
    source.push(chunk);
  });
  source.push(null); //Note: forEach synchronous call
};

describe('m-throttle-stream', function () {
  it('should throttle buffers, each having length less than bufferSize', function (done) {
    assertPipe(
      [
        new Buffer('aa'),
        new Buffer('bb'),
        new Buffer('cc')
      ], {
        bufferSize: 10,
        maxTimeout: 100
      },
      done
    );
  });

  it('should throttle buffers, one having length greater than bufferSize', function (done) {
    assertPipe(
      [
        new Buffer('aa'),
        new Buffer('bbcdef'),
        new Buffer('ccde')
      ], {
        bufferSize: 5,
        maxTimeout: 100
      },
      done
    );
  });

  it('should throttle objects, each having length less than bufferSize', function (done) {
    assertPipe(
      [
        {a: 1},
        {b: 2},
        {c: 3}
      ], {
        bufferSize: 10,
        maxTimeout: 100,
        objectMode: true
      },
      done
    );
  });

  it('should throttle objects, one having length greater than bufferSize', function (done) {
    assertPipe(
      [
        {a: 1},
        {b1: 21, b2: 22, b3: 23},
        {c: 3}
      ], {
        bufferSize: 10,
        maxTimeout: 100,
        objectMode: true
      },
      done
    );
  });

  it('should throttle with strings, each having length less than bufferSize', function (done) {
    assertPipe(
      [
        'aa',
        'bb',
        'cc'
      ], {
        bufferSize: 10,
        maxTimeout: 100,
        encoding: 'utf8'
      },
      done
    );
  });

  it('should throttle strings, one having length greater than bufferSize', function (done) {
    assertPipe(
      [
        'aa',
        'bbcdef',
        'ccde'
      ], {
        bufferSize: 5,
        maxTimeout: 100,
        encoding: 'utf8'
      },
      done
    );
  });
});
